﻿using System;

namespace ShopAPI.Models
{
    public class OrderProductModel
    {
        public Guid Id { get; set; }

        public int Quantity { get; set; }

        public decimal Price { get; set; }

        public Guid ProductId { get; set; }

        public Guid OrderId { get; set; }
    }
}
