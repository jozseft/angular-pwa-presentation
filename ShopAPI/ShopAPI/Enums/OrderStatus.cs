﻿namespace ShopAPI.Enums
{
    public enum OrderStatus
    {
        InProgress = 1,
        Checkout = 2,
        Processing = 3,
        AwaitingShipment = 4,
        AwaitingPickup = 5,
        Completed = 6
    }
}
