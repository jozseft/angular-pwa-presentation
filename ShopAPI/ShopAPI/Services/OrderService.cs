﻿using ShopAPI.Data.Entities;
using ShopAPI.Data.Repositories;
using ShopAPI.Enums;
using ShopAPI.Models;
using System;
using System.Collections.Generic;

namespace ShopAPI.Services
{
    public interface IOrderService 
    {
        OrderProduct AddOrderProduct(OrderProductModel orderProductModel);

        Order GetOrderById(Guid orderId);

        List<Product> GetShoppingCart();

        void OrderCheckout(Guid orderId);

        void AddOrUpdateOrder(Order order);

        Order GetActiveOrder();

        int GetNumberOfOrderedProducts();
    }

    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;

        public OrderService(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public List<Product> GetShoppingCart() 
        { 
            List<Product> products = new List<Product>();

            Order order = _orderRepository.GetActiveOrder();

            if (order != null) 
            {
                products = _orderRepository.GetShoppingCartByOrderId(order.Id);
            }

            return products;
        }

        public OrderProduct AddOrderProduct(OrderProductModel orderProductModel) 
        {
            if (orderProductModel.OrderId == Guid.Empty) 
            { 
                Order newOrder = CreateOrder();
                orderProductModel.OrderId = newOrder.Id;
            }
                
            OrderProduct newOrderProduct = new OrderProduct 
            {
                Id = orderProductModel.Id,
                ProductId = orderProductModel.ProductId,
                Price = orderProductModel.Price,
                Quantity = orderProductModel.Quantity,
                OrderId = orderProductModel.OrderId
            };

            _orderRepository.SaveOrderProduct(newOrderProduct);

            return newOrderProduct;
        }

        private Order CreateOrder()
        {
            Order newOrder = new Order
            {
                Id = Guid.NewGuid(),
                Address = "address",
                PhoneNumber = "056456",
                status = OrderStatus.InProgress
            };

            _orderRepository.SaveOrder(newOrder);

            return newOrder;
        }

            public Order GetOrderById(Guid orderId) 
        {
            return _orderRepository.GetOrderById(orderId);
        }

        public void OrderCheckout(Guid orderId) 
        { 
            _orderRepository.UpdateOrderStatus(orderId, OrderStatus.Checkout);
        }

        public void AddOrUpdateOrder(Order newOrder) 
        { 
            _orderRepository.AddOrUpdateOrder(newOrder);
        }

        public Order GetActiveOrder() 
        {
            return _orderRepository.GetActiveOrder();
        }

        public int GetNumberOfOrderedProducts()
        {
            Order order = _orderRepository.GetActiveOrder();

            if (order != null)
            {
                return _orderRepository.GetNumberOfOrderedProductsByOrderId(order.Id);
            }

            return 0;
        }
    }
}
