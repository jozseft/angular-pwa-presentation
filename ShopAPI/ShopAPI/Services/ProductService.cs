﻿using ShopAPI.Data;
using ShopAPI.Data.Entities;
using ShopAPI.Helpers;
using System.Collections.Generic;
using System.Linq;
using System;
using ShopAPI.Data.Repositories;

namespace ShopAPI.Services
{
    public interface IProductService 
    {
        void AddDataToDB();

        List<Product> GetAllProducts();

        Product GetProductById(Guid id);
    }

    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public void AddDataToDB()
        {
            using (ShopAPIDBContext dbContext = new ShopAPIDBContext()) 
            {
                foreach (Product product in ProductsMocks.ProductList()) 
                {
                    dbContext.Products.Add(product);
                    dbContext.SaveChanges();
                }
            }
        }

        public List<Product> GetAllProducts()
        {
            return _productRepository.GetAllProducts();
        }

        public Product GetProductById(Guid id)
        {
            return _productRepository.GetProductById(id);
        }
    }
}
