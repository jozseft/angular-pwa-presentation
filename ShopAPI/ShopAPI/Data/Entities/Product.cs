﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShopAPI.Data.Entities
{
    public class Product
    {
        [Key]
        public Guid Id { get; set; }
        
        public string Name {  get; set; }

        public string Description {  get; set; }

        public string ShortDescription { get; set; }

        public string Image { get; set; }

        public double Price {  get; set; }

        public int Stock { get; set; }
    }
}
