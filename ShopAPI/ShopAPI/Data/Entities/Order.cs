﻿using ShopAPI.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ShopAPI.Data.Entities
{
    public class Order
    {
        [Key]
        public Guid Id { get; set; }

        public OrderStatus status { get; set; }

        public string Address { get; set; }

        public string PhoneNumber { get; set; }

        public ICollection<OrderProduct> OrderProducts { get; set; }
    }
}
