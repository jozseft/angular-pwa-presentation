﻿using ShopAPI.Data.Entities;
using ShopAPI.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ShopAPI.Data.Repositories
{
    public interface IOrderRepository 
    {
        Order GetActiveOrder();

        void SaveOrder(Order order);

        void SaveOrderProduct(OrderProduct orderProduct);

        Order GetOrderById(Guid orderId);

        List<Product> GetShoppingCartByOrderId(Guid orderId);

        Order UpdateOrderStatus(Guid orderId, OrderStatus status);

        Order AddOrUpdateOrder(Order order);

        int GetNumberOfOrderedProductsByOrderId(Guid orderId);
    }

    public class OrderRepository : IOrderRepository
    {
        public List<Product> GetShoppingCartByOrderId(Guid orderId) 
        {
            using (ShopAPIDBContext dbContext = new ShopAPIDBContext())
            {
                return dbContext.OrderProducts.Where(op => op.OrderId == orderId).Select(op => op.Product).ToList();
            }
        }

        public void SaveOrder(Order order)
        {
            using (ShopAPIDBContext dbContext = new ShopAPIDBContext())
            {
                dbContext.Orders.Add(order);
                dbContext.SaveChanges();
            }
        }

        public void SaveOrderProduct(OrderProduct orderProduct)
        {
            using (ShopAPIDBContext dbContext = new ShopAPIDBContext())
            {
                dbContext.OrderProducts.Add(orderProduct);
                dbContext.SaveChanges();
            }
        }

        public Order GetActiveOrder()
        {
            using (ShopAPIDBContext dbContext = new ShopAPIDBContext())
            {
                return dbContext.Orders.FirstOrDefault(o => o.status == OrderStatus.InProgress);
            }
        }

        public Order GetOrderById(Guid orderId)
        {
            using (ShopAPIDBContext dbContext = new ShopAPIDBContext())
            {
                return dbContext.Orders.FirstOrDefault(o => o.Id == orderId);
            }
        }

        public Order UpdateOrderStatus(Guid orderId, OrderStatus status)
        {
            using (ShopAPIDBContext dbContext = new ShopAPIDBContext())
            {
                Order dbOrder =  dbContext.Orders.FirstOrDefault(o => o.Id == orderId);

                if (dbOrder != null) 
                { 
                    dbOrder.status = status;
                    dbContext.SaveChanges();
                }

                return dbOrder;
            }
        }

        public Order AddOrUpdateOrder(Order order)
        {
            using (ShopAPIDBContext dbContext = new ShopAPIDBContext())
            {
                Order dbOrder = dbContext.Orders.FirstOrDefault(o => o.Id == order.Id);

                if (dbOrder != null)
                {
                    dbOrder.status = order.status;
                    dbOrder.Address = order.Address;
                    dbOrder.PhoneNumber = order.PhoneNumber;
                }
                else 
                {
                    dbContext.Orders.Add(order);           
                }

                dbContext.SaveChanges();
                return dbOrder;
            }
        }

        public int GetNumberOfOrderedProductsByOrderId(Guid orderId) 
        {
            using (ShopAPIDBContext dbContext = new ShopAPIDBContext())
            {
                return dbContext.OrderProducts.Count(o => o.OrderId == orderId);
            }
        }
    }
}
