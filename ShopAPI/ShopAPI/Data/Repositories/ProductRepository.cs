﻿using ShopAPI.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ShopAPI.Data.Repositories
{
    public interface IProductRepository
    {
        List<Product> GetAllProducts();

        Product GetProductById(Guid id);
    }

    public class ProductRepository : IProductRepository
    {
        public List<Product> GetAllProducts()
        {
            using (ShopAPIDBContext dbContext = new ShopAPIDBContext())
            {
                return dbContext.Products.ToList();
            }
        }

        public Product GetProductById(Guid id)
        {
            using (ShopAPIDBContext dbContext = new ShopAPIDBContext())
            {
                return dbContext.Products.FirstOrDefault(p => p.Id == id);
            }
        }
    }
}
