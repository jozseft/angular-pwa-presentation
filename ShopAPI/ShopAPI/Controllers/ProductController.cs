﻿using Microsoft.AspNetCore.Mvc;
using ShopAPI.Data.Entities;
using ShopAPI.Extensions;
using ShopAPI.Helpers;
using ShopAPI.Models;
using ShopAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ShopAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpGet("all")]
        public List<Product> Get()
        {
            List<Product> products = _productService.GetAllProducts();
            products.Shuffle();

            return products;
        }

        [HttpGet("details/{productId:Guid}")]
        public Product Get(Guid productId)
        {
            return _productService.GetProductById(productId);
        }

        [HttpGet("add-data-db")]
        public void AddDataDb()
        {
            _productService.AddDataToDB();
        }
    }
}
