﻿using Microsoft.AspNetCore.Mvc;
using ShopAPI.Data.Entities;
using ShopAPI.Helpers;
using ShopAPI.Models;
using ShopAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ShoppingCartController : ControllerBase
    {
        private readonly IOrderService _orderService;

        public ShoppingCartController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [Authorize]
        [HttpGet]
        public List<Product> Get()
        {
            return _orderService.GetShoppingCart();
        }

        [Authorize]
        [HttpGet("order/{orderId:Guid}")]
        public Order GetOrder(Guid orderId)
        {
            return _orderService.GetOrderById(orderId);
        }

        [Authorize]
        [HttpPost("order-product/add")]
        public OrderProduct AddOrderProduct(OrderProductModel orderProductModel)
        {
            return _orderService.AddOrderProduct(orderProductModel);
        }

        [Authorize]
        [HttpPost("order/{orderId:Guid}/checkout")]
        public IActionResult OrdeCheckout(Guid orderId)
        {
            _orderService.OrderCheckout(orderId);

            return Ok();
        }

        [Authorize]
        [HttpPost("order/save")]
        public IActionResult SaveOrder(Order order)
        {
            _orderService.AddOrUpdateOrder(order);

            return Ok();
        }

        [Authorize]
        [HttpGet("order/active")]
        public IActionResult GetActiveOrder()
        {
            return Ok(_orderService.GetActiveOrder());
        }

        [Authorize]
        [HttpGet("order/number-of-ordered-products")]
        public IActionResult GetNumberOfOrderedProducts()
        {
            return Ok(_orderService.GetNumberOfOrderedProducts());
        }
    }
}
