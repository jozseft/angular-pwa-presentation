﻿using ShopAPI.Data.Entities;
using ShopAPI.Models;
using System;
using System.Collections.Generic;

namespace ShopAPI.Helpers
{
    public static class ProductsMocks
    {
        private static string _description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent tincidunt sit amet dolor ac ornare. Morbi semper, enim sed malesuada faucibus, mauris lacus ultrices nisl, at auctor metus ex eu sem. Nam commodo libero eget malesuada pellentesque. Mauris libero nisi, luctus ac elit at, imperdiet sagittis est. Aliquam ut rutrum elit. Donec ultricies fermentum scelerisque. Praesent id vehicula massa. Suspendisse a tempus erat. Donec bibendum urna non erat euismod pharetra sit amet ut mi.Donec suscipit tincidunt augue, ac vestibulum urna egestas vel.Donec ut tellus tincidunt, sollicitudin arcu vel, semper est. In quam felis, tristique ac imperdiet sed, aliquet nec nibh.Duis vulputate tincidunt leo a egestas. Suspendisse mollis cursus sollicitudin. Fusce id porta dui, ac viverra mi.Donec pulvinar volutpat sollicitudin. Suspendisse et nibh quis elit molestie fringilla posuere quis magna. Duis bibendum porta risus, vel sodales tortor tincidunt eget.Aliquam tincidunt orci erat, id porttitor neque sagittis sed.";
        private static string _shortDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent tincidunt sit amet dolor ac ornare.";


        public static List<Product> ProductList() 
        {
            List<Product> products = new List<Product>();

            products.Add(new Product { 
                Id = Guid.Parse("d10ab03a-0c26-4989-8dc0-f1be94379584"),
                Name = "Shoes 1",
                Description = _description,
                ShortDescription = _shortDescription,
                Image = "brown-shoes.jpg",
                Price = 10,
                Stock = 10
            });

            products.Add(new Product
            {
                Id = Guid.Parse("fa20747a-9d6e-4fc8-a068-19306dce826e"),
                Name = "Shoes 2",
                Description = _description,
                ShortDescription = _shortDescription,
                Image = "pink-shoes.jpg",
                Price = 12,
                Stock = 5
            });

            products.Add(new Product
            {
                Id = Guid.Parse("4dd51d7b-14f3-4c3b-a098-f6f7f49b5536"),
                Name = "Shoes 3",
                Description = _description,
                ShortDescription = _shortDescription,
                Image = "feet-pink.jpg",
                Price = 8,
                Stock = 20
            });

            products.Add(new Product
            {
                Id = Guid.Parse("8d4a15e9-1e57-425b-88a7-877c002f454e"),
                Name = "Shoes 4",
                Description = _description,
                ShortDescription = _shortDescription,
                Image = "feet-white.jpg",
                Price = 8,
                Stock = 20
             });

            return products;
        }
    }
}
