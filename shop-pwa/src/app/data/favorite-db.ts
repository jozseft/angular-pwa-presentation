import { Injectable } from "@angular/core";
import { Favorite } from "../models/favorite.model";
import { DBService } from "./app-db";

@Injectable({ providedIn: 'root' }) 
export class FavoriteIDB extends DBService {

    constructor() { 
        super();
    }

    addFavorite(favorite: Favorite) {
        this.favorites.put(favorite).then(() => { });
    }

    removeFavorite(id: string) {
        this.favorites.delete(id);
    }

    async getFavoriteByProductId(productId: string): Promise<Favorite> {
        return await this.transaction('rw', this.favorites, async () => {
            // Transaction block
            return await this.favorites.where('productId').equals(productId).first();
          });
    }
}