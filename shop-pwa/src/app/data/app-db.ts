import { Injectable } from "@angular/core";
import Dexie from "dexie";
import { Sync } from "../enums/sync.enum";
import { Favorite } from "../models/favorite.model";
import { OrderProduct } from "../models/order-product";
import { Order } from "../models/order.model";
import { Product } from "../models/product.model";

@Injectable({ providedIn: 'root' }) 
export class DBService extends Dexie {
    private readonly _dbVersion = 6;
    favorites: Dexie.Table<Favorite, string>;
    orders: Dexie.Table<Order, string>;
    orderProducts: Dexie.Table<OrderProduct, string>;

    constructor() { 
        super("shopDB");
        
        //
        // Define tables and indexes
        // (Here's where the implicit table props are dynamically created)
        //
        this.version(this._dbVersion).stores({
            favorites: 'id, productId, note',
            orders: 'id, status, address, phoneNumber, isSynced',
            orderProducts: 'id, productId, productName, quantity, price, orderId, isSynced',
        });
        
        this.favorites.mapToClass(Favorite);
        this.orders.mapToClass(Order);
        this.orderProducts.mapToClass(OrderProduct);
    }
}