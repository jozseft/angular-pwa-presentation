import { Injectable } from "@angular/core";
import { OrderStatus } from "../enums/order-status.enum";
import { Sync } from "../enums/sync.enum";
import { OrderProduct } from "../models/order-product";
import { Order } from "../models/order.model";
import { DBService } from "./app-db";

@Injectable({ providedIn: 'root' }) 
export class OrderIDB extends DBService {

    constructor() { 
        super();
    }

    addOrder(order: Order) {
        this.orders.put(order).then(() => { });
    }

    async getCurrentOrder(): Promise<Order> {
        return await this.transaction('r', this.orders, async () => {
            // Transaction block
            return await this.orders.where('status').equals(OrderStatus.InProgress).first();
          });
    }

    addOrderProduct(orderProduct: OrderProduct) {
        return this.orderProducts.put(orderProduct);
    }

    async checkIfProductOrdered(productId: string): Promise<OrderProduct> {
        return await this.transaction('r', this.orderProducts, async () => {
            // Transaction block
            return await this.orderProducts.where('productId').equals(productId).first();
          });
    }

    async getCountProductOrdered(): Promise<number> {
        return await this.transaction('r', this.orderProducts, async () => {
            // Transaction block
            return await this.orderProducts.count();
          });
    }

    async checkNotSyncedProductOrders(): Promise<number> {
        return await this.transaction('r', this.orderProducts, async () => {
            // Transaction block
            return await this.orderProducts.where('isSynced').equals(Sync.NotSynced).count();
          });
    }

    async checkNotSyncedOrders(): Promise<number> {
        return await this.transaction('r', this.orders, async () => {
            // Transaction block
            return await this.orders.where('isSynced').equals(Sync.NotSynced).count();
          });
    }

    async getNotSyncedProductOrders(): Promise<OrderProduct[]> {
        return await this.transaction('r', this.orderProducts, async () => {
            // Transaction block
            return await this.orderProducts.where('isSynced').equals(Sync.NotSynced).toArray();
          });
    }

    async getNotSyncedOrder(): Promise<Order> {
        return await this.transaction('r', this.orders, async () => {
            // Transaction block
            return await this.orders.where('isSynced').equals(Sync.NotSynced).first();
          });
    }

    async updateOrderProductSync(orderProductId: string, sync: Sync): Promise<number> {
        return await this.transaction('rw', this.orderProducts, async () => {
            // Transaction block
            return await this.orderProducts.update(orderProductId, {
                'isSynced': sync
            })
          });
    }

    async updateOrderToCheckout(orderId: string): Promise<number> {
        return await this.transaction('rw', this.orders, async () => {
            // Transaction block
            return await this.orders.update(orderId, {
                'status': OrderStatus.Checkout
            })
          });
    }

    async updateOrderSync(orderId: string, sync: Sync): Promise<number> {
        return await this.transaction('rw', this.orders, async () => {
            // Transaction block
            return await this.orders.update(orderId, {
                'isSynced': sync
            })
          });
    }

    async deleteOrderedProducts(orderId: string): Promise<number> {
        return await this.transaction('rw', this.orderProducts, async () => {
            // Transaction block
            return await this.orderProducts.where('orderId').equals(orderId).delete();
        });
    }

    async deleteOrder(orderId: string): Promise<number> {
        return await this.transaction('rw', this.orders, async () => {
            // Transaction block
            return await this.orders.where('id').equals(orderId).delete();
        });
    }

    async getOrderedProducts(orderId: string): Promise<OrderProduct[]> {
        return await this.transaction('r', this.orderProducts, async () => {
            // Transaction block
            return await this.orderProducts.where('orderId').equals(orderId).toArray();
        });
    }
}