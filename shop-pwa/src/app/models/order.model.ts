import { OrderStatus } from "../enums/order-status.enum";
import { Sync } from "../enums/sync.enum";

export class Order {
    id: string;
    status: OrderStatus;
    address: string;
    phoneNumber: string;
    isSynced: Sync;
}