export class Favorite {
    id?: string;
    productId: string;
    note?: string;
}