import { Sync } from "../enums/sync.enum";

export class OrderProduct {
    id: string;
    productName: string;
    productId: string;
    quantity: number;
    price: number;
    orderId?: string;
    isSynced: Sync;
}