import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { imagebaseURL } from '../constants';
import { Product } from '../models/product.model';
import { ShopService } from '../api/shop.service';
import { FavoriteService } from '../services/favorite.service';
import { OrderService } from '../services/order.service';
import { OrderProduct } from '../models/order-product';
import { ShareService } from '../services/share.service';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {
  productId: string;
  product: Product;
  imageURL: string;
  favoriteId: string;
  ordered: boolean = false;
  isLoggedIn: boolean = false;

  constructor(private activatedRoute: ActivatedRoute,
    private shopService: ShopService,
    private favoriteService: FavoriteService,
    private orderService: OrderService,
    private shareService: ShareService,
    private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
    this.productId = this.activatedRoute.snapshot.params['id'];
    this.isLoggedIn = !!this.authenticationService.currentUserValue;
    this.checkIfFavorite();
    this.checkIfOrdered();
    this.getDetails();
  }

  getDetails() {
    this.shopService.getProductById(this.productId).subscribe((data: Product) => {
      this.product = data;
      this.imageURL = imagebaseURL + this.product.image;
    });
  }

  checkIfFavorite() {
    this.favoriteService.checkIfFavorite(this.productId).then((favoriteId: string) => {
      this.favoriteId = favoriteId;
    });
  }

  setFavorite(event: MouseEvent) {
    event.stopPropagation();

    if(this.favoriteId) {
      this.favoriteService.removeFavorite(this.favoriteId);
      this.favoriteId = null;
    }
    else {
      this.favoriteId = this.favoriteService.addFavorite(this.productId);
    }
  }

  checkIfOrdered() {
    this.orderService.checkIfOrdered(this.productId).then((data: OrderProduct) => {
      this.ordered = !!data;
    });
  }

  order(event: MouseEvent) {
    event.stopPropagation();

    this.orderService.orderProduct(this.product);
    this.ordered = true;
  }

  share(event: MouseEvent) {
    event.stopPropagation();
    this.shareService.shareProduct(this.product);
  }
}
