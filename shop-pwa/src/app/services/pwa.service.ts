import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { SwUpdate } from "@angular/service-worker";
import { AppModule } from "../app.module";

@Injectable()
export class PWAService {
    constructor(private swUpdate: SwUpdate, private _snackBar: MatSnackBar) {
        this.checkNewerVersion();
    }

    checkNewerVersion() {
        this.swUpdate.available.subscribe(() => {
            let snackBar = this._snackBar.open('A new version is available!', 'Update');

            snackBar.onAction().subscribe(() => {
                location.reload();
            });
        });
    }
}