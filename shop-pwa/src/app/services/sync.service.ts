import { Injectable, EventEmitter } from "@angular/core";
import { OrderIDB } from "../data/order-db";
import { AppServiceEnum } from "../enums/app-service.enum";
import { OrderProduct } from "../models/order-product";
import { AppService } from "./app.service";
import { ShopService } from "../api/shop.service";
import { Sync } from "../enums/sync.enum";
import { Order } from "../models/order.model";
import { OrderStatus } from "../enums/order-status.enum";
import { OrderService } from "./order.service";

@Injectable({ providedIn: 'root' }) 
export class SyncService {
    constructor(private orderIDB: OrderIDB,
        private shopService: ShopService,
        private appService: AppService,
        private orderService: OrderService) { }

    needSync(): Promise<number[]> {
        const list = [
            this.orderIDB.checkNotSyncedOrders(),
            this.orderIDB.checkNotSyncedProductOrders()
        ];

        return Promise.all(list);
    }

    sync() {
        this.orderIDB.getNotSyncedOrder()
        .then((order: Order) => {
            if (order) {
                return this.syncOrder(order);            
            }

            return Promise;
        })
        .then(() => {
           return this.syncOrderedProducts();
        })
        .then(() => {
            this.appService.appEvent.emit(AppServiceEnum.SyncSucceeded);
        })
    }

    syncOrderedProducts() {
        return this.orderIDB.getNotSyncedProductOrders().then((products: OrderProduct[]) => {
            let p: Promise<void>[] = [];
            products.forEach((orderProduct: OrderProduct) => {
                p.push(this.syncOrderProduct(orderProduct));
            });

            return Promise.all(p);
        })
    }

    syncOrderProduct(orderProduct: OrderProduct) {
        return this.shopService.addOrderProduct(orderProduct).toPromise().then((orderProductData: OrderProduct) => {
            this.orderIDB.updateOrderProductSync(orderProductData.id, Sync.Synced);
        });
    }

    syncOrder(order: Order): Promise<any> {
        if (order.status == OrderStatus.Checkout) {
            return this.shopService.orderCheckout(order.id).toPromise().then(() => {
                this.orderService.deleteOrderAndEmitCheckout(order.id);

                return Promise;
            });
        }
        else
        {
            return this.shopService.saveOrder(order).toPromise().then(() => {
                this.orderIDB.updateOrderSync(order.id, Sync.Synced);

                return Promise;
            });
        }
    }
}