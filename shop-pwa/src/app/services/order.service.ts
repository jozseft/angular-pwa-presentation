import { Injectable, EventEmitter } from "@angular/core";
import { ShopService } from "../api/shop.service";
import { OrderIDB } from "../data/order-db";
import { AppServiceEnum } from "../enums/app-service.enum";
import { Sync } from "../enums/sync.enum";
import { Order } from "../models/order.model";
import { Product } from "../models/product.model";
import { AppService } from "./app.service";
import { v4 as uuid } from 'uuid';
import { OrderProduct } from "../models/order-product";
import { OrderStatus } from "../enums/order-status.enum";

@Injectable({ providedIn: 'root' }) 
export class OrderService {
    
    constructor(private shopService: ShopService,
        private orderIDB: OrderIDB,
        private appService: AppService) { }

    checkIfOrdered(productId: string): Promise<OrderProduct> {
      return this.orderIDB.checkIfProductOrdered(productId).then((data) => {
        return data;
      });
    }

    orderProduct(product: Product) {
        let orderProduct: OrderProduct = {
            id: uuid(),
            productId: product.id,
            productName: product.name,
            price: product.price,
            quantity: 1,
            isSynced: Sync.NotSynced
          }; 
          
          this.orderIDB.getCurrentOrder().then((orderData: Order) => {
            let existOrder = !!orderData;
            if (existOrder) {
              orderProduct.orderId = orderData.id;
              this.addOrderProduct(orderProduct);
            }
            else 
            {
                this.saveOrder(this.createOrder()).then((orderId: string) => {
                     orderProduct.orderId = orderId;
                     this.addOrderProduct(orderProduct);
                });
            }
          });
    }

    addOrderProduct(orderProduct: OrderProduct) {
        this.shopService.addOrderProduct(orderProduct).subscribe((orderProductData: OrderProduct) => {
          orderProduct.isSynced = Sync.Synced;
          this.orderIDB.addOrderProduct(orderProduct).then(() => { 
            this.appService.appEvent.emit(AppServiceEnum.AddedToCart);
          });
        },
        () => {
          this.orderIDB.addOrderProduct(orderProduct).then(() => {
            this.appService.appEvent.emit(AppServiceEnum.NeedSync);
            this.appService.appEvent.emit(AppServiceEnum.AddedToCart);
          });
        });
    }
    
    saveOrder(newOrder: Order): Promise<string> { 
        return this.shopService.saveOrder(newOrder).toPromise().then(() => {
            this.orderIDB.updateOrderSync(newOrder.id, Sync.Synced);
            this.orderIDB.addOrder(newOrder);

            return Promise.resolve(newOrder.id);
        },
        () => {
            this.orderIDB.addOrder(newOrder);
            this.appService.appEvent.emit(AppServiceEnum.NeedSync);

            return Promise.resolve(newOrder.id);
        });
    }
  
    createOrder(): Order {
        return {
            id: uuid(),
            status: OrderStatus.InProgress,
            phoneNumber: '056456',
            address: 'address',
            isSynced: Sync.NotSynced
        }
    }

    deleteOrderAndEmitCheckout(orderId: string) {
        this.orderIDB.deleteOrderedProducts(orderId);
        this.orderIDB.deleteOrder(orderId);
        this.appService.appEvent.emit(AppServiceEnum.Checkout);
    }

    getOrderedProducts(orderId: string) {
      return this.orderIDB.getOrderedProducts(orderId);
    }
}