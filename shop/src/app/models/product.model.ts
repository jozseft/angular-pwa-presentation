export class Product {
    id: string;
    name: string;
    description?: string;
    shortDescription?: string;
    image?: string;
    price: number;
    stock?: number;
} 