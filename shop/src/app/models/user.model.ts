export interface User {
    firstName: string;
    id: number;
    lastName: string;
    token: string;
    username: string;
}