import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Product } from "../models/product.model";
import { baseURL } from "../constants";
import { OrderProduct } from "../models/order-product";
import { Order } from "../models/order.model";

@Injectable({ providedIn: 'root' }) 
export class ShopService {

    constructor(private http: HttpClient) { }

    getProducts(): Observable<Product[]> {
        return this.http.get<Product[]>(`${baseURL}/api/Product/all`);
    }

    getProductById(id: string): Observable<Product> {
        return this.http.get<Product>(`${baseURL}/api/Product/details/${id}`);
    }

    getShoppingCart(): Observable<Product[]> {
        return this.http.get<Product[]>(`${baseURL}/api/ShoppingCart`);
    }

    addOrderProduct(orderProduct: OrderProduct) {
        return this.http.post(`${baseURL}/api/ShoppingCart/order-product/add`, orderProduct);
    }

    getOrderById(orderId: string) {
        return this.http.get(`${baseURL}/api/ShoppingCart/order/${orderId}`);
    }

    orderCheckout(orderId: string) {
        return this.http.post(`${baseURL}/api/ShoppingCart/order/${orderId}/checkout`, null);
    }

    saveOrder(order: Order) {
        return this.http.post(`${baseURL}/api/ShoppingCart/order/save`, order);
    }

    getActiveOrder() {
        return this.http.get(`${baseURL}/api/ShoppingCart/order/active`);
    }

    getNumberOfOrderedProducts() {
        return this.http.get(`${baseURL}/api/ShoppingCart/order/number-of-ordered-products`);
    }
}