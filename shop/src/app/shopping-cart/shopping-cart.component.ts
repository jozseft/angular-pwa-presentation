import { Component, OnInit } from '@angular/core';
import { Product } from '../models/product.model';
import { ShopService } from '../api/shop.service';
import { OrderIDB } from '../data/order-db';
import { Order } from '../models/order.model';
import { OrderService } from '../services/order.service';
import { Sync } from '../enums/sync.enum';
import { AppServiceEnum } from '../enums/app-service.enum';
import { AppService } from '../services/app.service';
import { OrderStatus } from '../enums/order-status.enum';
import { OrderProduct } from '../models/order-product';

@Component({
  selector: 'shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss']
})
export class ShoppingCartComponent implements OnInit {
  products: Product[] = [];
  orderId: string;
  isCheckout: boolean = false;
  fromStorage: boolean = false;
  displayedColumns: string[] = ['name', 'price'];

  constructor(private shopService: ShopService,
    private orderIDB: OrderIDB,
    private orderService: OrderService,
    private appService: AppService) { }

  ngOnInit(): void {
    this.getOrder();
  }

  getOrder() {
    this.shopService.getActiveOrder().subscribe((orderData: Order) => {
      if (orderData) {
        this.orderId = orderData.id;
        this.getShoppingCart();
      }
    });

    // this.orderIDB.getCurrentOrder().then((order: Order) => {
    //   if (order) {
    //     this.orderId = order.id;
    //     this.isCheckout = order.status == OrderStatus.Checkout;
    //     this.getShoppingCart();
    //   }
    // });
  }

  getShoppingCart() {
    this.shopService.getShoppingCart().subscribe((data: Product[]) => {
      this.products = data;
    },
    () => {
      this.orderService.getOrderedProducts(this.orderId).then((data: OrderProduct[]) => {
        let prods: Product[] = [];
        if (data) {
          data.forEach((op: OrderProduct) => {
            let p: Product = {
              id: op.productId,
              price: op.price,
              name: op.productName
            };

            prods.push(p);
          });

          this.products = prods;
          this.fromStorage = true;
        }
      });
    });
  }

  checkoutOrder() {
    this.shopService.orderCheckout(this.orderId).subscribe(() => {
      // this.orderService.deleteOrderAndEmitCheckout(this.orderId);
      this.orderId = null;
    },
    () => {
        // this.isCheckout = true;
        // this.orderIDB.updateOrderToCheckout(this.orderId);
        // this.orderIDB.updateOrderSync(this.orderId, Sync.NotSynced);
        // this.appService.appEvent.emit(AppServiceEnum.NeedSync);
    });
  }
}
