import { Injectable } from "@angular/core";
import { Product } from "../models/product.model";

@Injectable({ providedIn: 'root' }) 
export class ShareService {

    shareProduct(product: Product) {
        if (navigator.share) {
            navigator.share({
                title: product.name,
                text: product.name + ' ' + product.shortDescription,
                //url
            })
            .then(() => {
                console.log('shared');
            })
            .catch(() => {
                console.log('share failed');
            });         
        }
    }
}