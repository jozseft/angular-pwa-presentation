import { Injectable } from "@angular/core";
import { FavoriteIDB } from "../data/favorite-db";
import { v4 as uuid } from 'uuid';
import { Favorite } from "../models/favorite.model";

@Injectable({ providedIn: 'root' }) 
export class FavoriteService {


    constructor(private favoriteDB: FavoriteIDB) { }

    addFavorite(productId: string): string {
        let favorite: Favorite = {
            id: uuid(),
            productId: productId,
            note: 'test'
        };
      
        this.favoriteDB.addFavorite(favorite);

        return favorite.id;
    }

    removeFavorite(favoriteId: string) {
        this.favoriteDB.removeFavorite(favoriteId);
    }

    checkIfFavorite(favoriteId: string): Promise<string> {
        return this.favoriteDB.getFavoriteByProductId(favoriteId).then((data: Favorite) => {
          return data?.id;
        });
    }
}