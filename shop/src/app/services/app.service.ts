import { Injectable, EventEmitter } from "@angular/core";
import { AppServiceEnum } from "../enums/app-service.enum";

@Injectable({ providedIn: 'root' }) 
export class AppService {
    appEvent: EventEmitter<AppServiceEnum> = new EventEmitter<AppServiceEnum>();

}