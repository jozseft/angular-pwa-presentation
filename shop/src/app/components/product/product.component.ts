import { Component, Input, OnInit } from '@angular/core';
import { imagebaseURL } from 'src/app/constants';
import { OrderProduct } from 'src/app/models/order-product';
import { Product } from 'src/app/models/product.model';
import { FavoriteService } from 'src/app/services/favorite.service';
import { OrderService } from 'src/app/services/order.service';
import { ShareService } from 'src/app/services/share.service';

@Component({
  selector: 'product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  @Input() product: Product;
  @Input() isLoggedIn: boolean = false;

  imageURL: string;
  favoriteId: string;
  ordered: boolean = false;

  constructor(private orderService: OrderService,
    private favoriteService: FavoriteService,
    private shareService: ShareService) { }

  ngOnInit(): void {
    this.setImageURL();
    this.checkIfFavorite();
  }

  setImageURL() {
    this.imageURL = imagebaseURL + this.product.image;
  }

  checkIfFavorite() {
    this.favoriteService.checkIfFavorite(this.product.id).then((favoriteId: string) => {
      this.favoriteId = favoriteId;
    });
  }

  setFavorite(event: MouseEvent) {
    event.stopPropagation();

    if(this.favoriteId) {
      this.favoriteService.removeFavorite(this.favoriteId);
      this.favoriteId = null;
    }
    else {
      this.favoriteId = this.favoriteService.addFavorite(this.product.id);
    }
  }

  order(event: MouseEvent) {
    event.stopPropagation();

    this.orderService.orderProduct(this.product);
  }

  share(event: MouseEvent) {
    event.stopPropagation();
    this.shareService.shareProduct(this.product);
  }
}
