import { Component, OnInit } from '@angular/core';
import { Product } from '../models/product.model';
import { ShopService } from '../api/shop.service';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {
  products: Product[] = [];
  isLoggedIn: boolean = false;
  error: any;
  
  constructor(private shopService: ShopService, 
    private authenticationService: AuthenticationService) { }


  ngOnInit(): void {
    this.isLoggedIn = !!this.authenticationService.currentUserValue;
    this.getProducts();
  }

  getProducts() {
    this.shopService.getProducts().subscribe((products: Product[]) => {
      this.products = products;
    },
    (error) => {
      this.error = error;
    });
  }
}
