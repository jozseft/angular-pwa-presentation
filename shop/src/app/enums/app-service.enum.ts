export enum AppServiceEnum {
    SyncSucceeded = 1,
    SyncFailed = 2,
    NeedSync = 3,
    Checkout = 4,
    AddedToCart = 6
}