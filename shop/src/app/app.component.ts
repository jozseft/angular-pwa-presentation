import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ShopService } from './api/shop.service';
import { DBService } from './data/app-db';
import { OrderIDB } from './data/order-db';
import { AppServiceEnum } from './enums/app-service.enum';
import { AppService } from './services/app.service';
import { AuthenticationService } from './services/authentication.service';
import { SyncService } from './services/sync.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'shop';
  imagePhoneHand: SafeResourceUrl = '';
  imageHappyWoman: SafeResourceUrl = '';
  isLoggedIn: boolean = false;
  countOrderedProducts: number = 0;
  isSynced: boolean = true;

  constructor(private domSanitizer: DomSanitizer, 
    private db: DBService,
    private authenticationService: AuthenticationService,
    private router: Router,
    private orderIDB: OrderIDB,
    private syncService: SyncService,
    private appService: AppService,
    private shopService: ShopService) { }

  ngOnInit() { 
    this.sanitizeImages();
    this.isLoggedIn = !!this.authenticationService.currentUserValue;

    if (this.isLoggedIn) {
      this.getNumberOfOrderedProducts();
      this.checkNeedsSync();
      this.listenForSyncing();
    }
  }

  getNumberOfOrderedProducts() {
    this.shopService.getNumberOfOrderedProducts().subscribe((data: number) => {
      this.countOrderedProducts = data;
    }); 

    // this.orderIDB.getCountProductOrdered().then((data: number) => {
    //   this.countOrderedProducts = data;
    // });
  }

  checkNeedsSync() {
    this.syncService.needSync().then((data) => {
      this.isSynced = !data.some(s => s > 0);
    });
  }

  syncData() {
    if (!this.isSynced) {
      this.syncService.sync();
    }
  }

  listenForSyncing () {
    this.appService.appEvent.subscribe((event: AppServiceEnum) => {
          switch(event) {
            case AppServiceEnum.SyncSucceeded:
              this.isSynced = true;
              break;
            case AppServiceEnum.NeedSync:
              this.isSynced = false;
              break;
            case AppServiceEnum.Checkout:
              this.countOrderedProducts = 0;
              break;
            case AppServiceEnum.AddedToCart:
              this.getNumberOfOrderedProducts();
              break;
          }
      });
  }

  login() {
    this.authenticationService.login('user', 'test').subscribe(() => {
      this.isLoggedIn = true;
      location.reload();
    });
  }

  sanitizeImages() {
    this.imagePhoneHand = this.domSanitizer.bypassSecurityTrustStyle('url(/assets/phone-hand.jpg)');
    this.imageHappyWoman = this.domSanitizer.bypassSecurityTrustStyle('url(/assets/happy-woman.jpg)');
  }

  logout() {
    this.authenticationService.logout();
    this.isLoggedIn = false;
    location.reload();
  }

  goToShoppingPage() {
    this.router.navigate(['shopping-cart']);
  }
}
